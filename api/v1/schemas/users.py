import arrow

from marshmallow import (
    Schema, 
    fields
)
from api.utils import (
    denormalize_phone_number
)

class DenormalizePhone(fields.Field):
    def _serialize(self, value, attr, obj):
        if value:
            return denormalize_phone_number(value)
        return value


class PermissionSchema(Schema):
    id = fields.Str()
    name = fields.Str()
    slug = fields.Str()
    description = fields.Str()
    created = fields.DateTime()
    updated = fields.DateTime()


class RoleSchema(Schema):
    id = fields.Str()
    name = fields.Str()
    slug = fields.Str()
    description = fields.Str()
    permissions = fields.Dict()
    created = fields.DateTime()
    updated = fields.DateTime()
    total_user = fields.Method('count_total_user')
    
    def count_total_user(self, obj):
        return obj.users.count()
        

class UserSchema(Schema):
    id = fields.Str()
    nid = fields.Str()
    email = fields.Str()
    phone = DenormalizePhone()
    status = fields.Str()
    created = fields.DateTime()
    updated = fields.DateTime()
    profile = fields.Nested('ProfileSchema')
    role = fields.Nested('RoleSchema', attribute='user_role')


class ProfileSchema(Schema):
    id = fields.Str()
    fullname = fields.Str()
    gender = fields.Str()
    dob = fields.Date()
    address = fields.Str()
    balance = fields.Int()
    point = fields.Int()
    timezone = fields.Str()


class BankAccountSchema(Schema):
    id = fields.Str()
    account_name = fields.Str()
    account_number = fields.Str()
    branch = fields.Str()
    bank_id = fields.Int()
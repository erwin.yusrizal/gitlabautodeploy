# -*- coding: utf-8 -*-
import arrow
import bleach
import time
import re
import requests

from datetime import (
    datetime,
    timedelta
)
from flask import (
    g,
    current_app,
    jsonify,
    request,
    make_response
)
from sqlalchemy import asc, desc
from sqlalchemy.sql.expression import func, and_, or_
from flask_restful import Resource
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    use_kwargs,
    parser
)
from api import cache, db
from api.v1 import api
from api.v1.models.users import(
    PermissionModel,
    RoleModel,
    UserModel,
    ProfileModel,
    BankAccountModel
)
from api.v1.schemas.users import(
    PermissionSchema,
    RoleSchema,
    UserSchema,
    ProfileSchema,
    BankAccountSchema
)
from api.utils import (
    normalize_phone_number,
    denormalize_phone_number
)
from api.utils.pagination import (
    Pagination
)
from api.decorators import(
    permission_required,
    current_user
)


class Users(Resource):

    '''
    @method GET
    @endpoint /v1/users
    @return user list
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required field')


    get_args = {
        'q': fields.Str(missing=None),
        'role': fields.Str(missing=None),
        'status': fields.Str(validate=validate.OneOf(choices=['unverified', 'verified', 'banned'], error='Invalid choice'), missing=None),
        'order': fields.Str(validate=validate.OneOf(choices=['asc', 'desc'], error='Invalid choice'), missing='asc'),
        'orderby': fields.Str(missing='id'),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @permission_required('user', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        q = args['q']
        role = args['role']
        status = args['status']
        order = args['order']
        orderby = args['orderby']
        page = args['page']
        perpage = args['perpage']

        total = UserModel.count_all(q, status, role)
        query = UserModel.get_all(q, status, role, order, orderby, page, perpage)

        schema = UserSchema(only=args['only'], exclude=args['exclude'], many=True)
        users = schema.dump(query).data
        pagination = Pagination('api.users', total, **args)

        return make_response(jsonify({
            'success': True,
            'message': '',
            'data': {
                'users': users,
                'pagination': pagination.paginate
            }
        }), 200)


    '''
    @method POST
    @endpoint /v1/users
    @return new user object
    ''' 

    def validate_phone(value):
        phone_match = re.search(r'^\(?(?:\+628|628|08)(?:\d{2,3})?\)?\d{2,4}?\d{2}?\d{2}$', value)

        if phone_match is None:
            raise ValidationError('Invalid phone number') 

    post_args = {
        'email': fields.Email(required=True, validate=[validate_required]),
        'phone': fields.Str(required=True, validate=[validate_required, validate_phone]),
        'status': fields.Str(validate=[
            validate.OneOf(choices=['unverified', 'verified', 'banned'], error="Invalid choice")
        ], missing='unverified'),        
        'role_id': fields.UUID(required=True, validate=[validate_required]),
        'fullname': fields.Str(required=True, validate=[
            validate_required, 
            validate.Regexp(r'^([a-zA-Z\s]{2,255})$', error='Full name must have 2 characters maximum. Only letters and spaces are allowed')
        ]),
        'gender': fields.Str(validate=[
            validate.OneOf(choices=['m', 'f'], error="Invalid choice")
        ], missing=None),
        'dob': fields.Date(missing=None),
        'address': fields.Str(required=True, validate=[validate_required]),
        'location_id': fields.UUID(missing=None),
        'photo_id': fields.UUID(missing=None),
        'timezone': fields.Str(validate=[
            validate.OneOf(choices=['Asia/Jakarta', 'Asia/Makassar', 'Asia/Jayapura'], error="Invalid choice")
        ], missing='Asia/Jakarta'), 
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @current_user()
    @permission_required('user', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        email = payload['email']
        phone = normalize_phone_number(payload['phone'])
        status = payload['status']
        fullname = bleach.clean(payload['fullname'], strip=True)
        role_id = payload['role_id']
        gender = payload['gender']
        dob = payload['dob']
        address = bleach.clean(payload['address'], strip=True) 
        location_id = payload['location_id']
        timezone = payload['timezone']
        photo_id = payload['photo_id']
        
        user_exist = UserModel.query.filter(or_(UserModel.email==email, UserModel.phone==phone)).first()

        if user_exist:
            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 409,
                    'messages': {
                        'user': ['Another user with this phone number or email address already exist']
                    }
                }
            }), 409)

        user = UserModel()
        user.email = email
        user.phone = phone
        user.status = status
        user.role_id = role_id

        db.session.add(user)

        profile = ProfileModel()
        profile.fullname = fullname
        profile.gender = gender
        profile.dob = dob
        profile.address = address
        profile.location_id = location_id
        profile.user_id = user.id

        try:
            db.session.commit()

            schema = UserSchema(only=args['only'], exclude=args['exclude'])
            result = schema.dump(user).data

            return make_response(jsonify({
                'success': True,
                'message': '',
                'data': {
                    'user': result
                }
            }), 201)

        except Exception as e:
            db.session.rollback()

            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 400,
                    'messages': {
                        'user': ['Something went wrong. Please try again later.']
                    }
                }
            }), 400)

api.add_resource(Users, '/users')


class User(Resource):

    '''
    @method GET
    @endpoint /v1/users/<string:user_id>
    @return user list
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required field')

    get_args = {
        'email': fields.Email(missing=None),
        'phone': fields.Str(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @permission_required('user', 'viewother')
    @use_args(get_args, locations=['query'])
    def get(self, args, user_id):

        phone = normalize_phone_number(args['phone']) if args['phone'] else None
        email = args['email']

        query = UserModel.get_by(user_id, phone, email)

        schema = UserSchema(only=args['only'], exclude=args['exclude'])
        user = schema.dump(query).data

        return make_response(jsonify({
            'success': True,
            'message': '',
            'data': {
                'user': user
            }
        }), 200)


    '''
    @method PUT
    @endpoint /v1/users/<string:user_id>
    @return updated user object
    ''' 

    def validate_phone(value):
        phone_match = re.search(r'^\(?(?:\+628|628|08)(?:\d{2,3})?\)?\d{2,4}?\d{2}?\d{2}$', value)

        if phone_match is None:
            raise ValidationError('Invalid phone number') 


    put_args = {
        'email': fields.Email(required=True, validate=[validate_required]),
        'phone': fields.Str(required=True, validate=[validate_required, validate_phone]),
        'status': fields.Str(validate=[
            validate.OneOf(choices=['unverified', 'verified', 'banned'], error="Invalid choice")
        ], missing='unverified'),        
        'role_id': fields.UUID(required=True, validate=[validate_required]),
        'fullname': fields.Str(required=True, validate=[
            validate_required, 
            validate.Regexp(r'^([a-zA-Z\s]{2,255})$', error='Full name must have 2 characters maximum. Only letters and spaces are allowed')
        ]),
        'gender': fields.Str(validate=[
            validate.OneOf(choices=['m', 'f'], error="Invalid choice")
        ], missing=None),
        'dob': fields.Date(missing=None),
        'address': fields.Str(required=True, validate=[validate_required]),
        'location_id': fields.UUID(missing=None),
        'timezone': fields.Str(validate=[
            validate.OneOf(choices=['Asia/Jakarta', 'Asia/Makassar', 'Asia/Jayapura'], error="Invalid choice")
        ], missing='Asia/Jakarta'), 
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @current_user()
    @permission_required('user', 'editother')
    @use_args(put_args, locations=['json'])
    def put(self, payload, user_id):

        user = UserModel.get_by(user_id)

        if not user:
            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['User not found or has been deleted']
                    }
                }
            }), 404)

        email = payload['email']
        phone = normalize_phone_number(payload['phone'])
        status = payload['status']
        fullname = bleach.clean(payload['fullname'], strip=True)
        role = payload['role']
        gender = payload['gender']
        dob = payload['dob']
        address = bleach.clean(payload['address'], strip=True) 
        location = payload['location']
        timezone = payload['timezone']
        
        user_exist = UserModel.query.filter(UserModel.id != user.id, or_(UserModel.email==email, UserModel.phone==phone)).first()

        if user_exist:
            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 409,
                    'messages': {
                        'user': ['Another user with this phone number or email address already exist']
                    }
                }
            }), 409)


        user.email = email
        user.phone = phone        
        user.status = status
        user.role_id = role_id            

        user.profile.fullname = fullname
        user.profile.gender = gender
        user.profile.dob = dob
        user.profile.address = address
        user.profile.company = company
        user.profile.location_id = location_id
        user.profile.timezone = timezone

        try:

            db.session.commit()

            schema = UserSchema(only=args['only'], exclude=args['exclude'])
            result = schema.dump(user).data

            return make_response(jsonify({
                'success': True,
                'message': '',
                'data': {
                    'user': result
                }
            }), 200)

        except Exception as e:
            db.session.rollback()

            return make_response(jsonify({
                'success': False,
                'code': 400,
                'messages': {
                    'user': ['Something went wrong. Please try again later.']
                }
            }), 400)


    '''
    @method DELETE
    @endpoint /v1/users/<string:user_id>
    @return deleted user object
    ''' 

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @current_user()
    @permission_required('user', 'deleteother')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, user_id):

        user = UserModel.get_by(user_id)

        if not user:
            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['User not found or has been deleted']
                    }
                }
            }), 404)

        schema = UserSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(user).data

        try:

            db.session.delete(user)
            db.session.commit()

            return make_response(jsonify({
                'success': True,
                'message': 'User has been deleted and cannot be undone.',
                'data': {
                    'user': result
                }
            }), 200)


        except Exception as e:

            db.session.rollback()

            return make_response(jsonify({
                'success': False,
                'code': 400,
                'messages': {
                    'user': ['Something went wrong. Please try again later.']
                }
            }), 400)
            

api.add_resource(User, '/users/<string:user_id>')


class UserAccount(Resource):

    '''
    @method GET
    @endpoint /v1/users/account
    @return user account object
    ''' 

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @current_user()
    @permission_required('user', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        user_id = request.headers.get('X-Consumer-Custom-Id')

        user = UserModel.get_by(user_id)

        if not user:
            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['User not found or has been deleted']
                    }
                }
            }), 404)

        schema = UserSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(user).data

        return make_response(jsonify({
            'success': True,
            'message': '',
            'data': {
                'user': result
            }
        }), 200)


    '''
    @method PUT
    @endpoint /v1/users/account
    @return updated user account object
    ''' 

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required field')

    def validate_phone(value):
        phone_match = re.search(r'^\(?(?:\+628|628|08)(?:\d{2,3})?\)?\d{2,4}?\d{2}?\d{2}$', value)

        if phone_match is None:
            raise ValidationError('Invalid phone number') 

    put_args = {
        'email': fields.Email(required=True, validate=[validate_required]),
        'phone': fields.Str(required=True, validate=[validate_required, validate_phone]),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @current_user()
    @permission_required('user', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload):

        user_id = request.headers.get('X-Consumer-Custom-Id')
        phone = normalize_phone_number(payload['phone'])
        email = payload['email']

        user = UserModel.get_by(user_id)

        if not user:
            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['User not found or has been deleted']
                    }
                }
            }), 404)

        user_exist = UserModel.query.filter(UserModel.id != user.id, or_(UserModel.email==email, UserModel.phone==phone)).first()

        if user_exist:
            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 409,
                    'messages': {
                        'user': ['Another user with this phone number or email address already exist']
                    }
                }
            }), 409)

        user.email = email

        schema = UserSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(user).data

        return make_response(jsonify({
            'success': True,
            'message': '',
            'data': {
                'user': result
            }
        }), 200)

api.add_resource(UserAccount, '/users/account')


class UserProfile(Resource):

    '''
    @method GET
    @endpoint /v1/users/profile
    @return user profile object
    ''' 

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @current_user()
    @permission_required('user', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        user_id = request.headers.get('X-Consumer-Custom-Id')

        user = UserModel.get_by(user_id)

        if not user:
            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['User not found or has been deleted']
                    }
                }
            }), 404)

        schema = UserSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(user).data

        return make_response(jsonify({
            'success': True,
            'message': '',
            'data': {
                'user': result
            }
        }), 200)


    '''
    @method PUT
    @endpoint /v1/users/profile
    @return updated user profile object
    ''' 

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required field')

    def validate_phone(value):
        phone_match = re.search(r'^\(?(?:\+628|628|08)(?:\d{2,3})?\)?\d{2,4}?\d{2}?\d{2}$', value)

        if phone_match is None:
            raise ValidationError('Invalid phone number') 

    put_args = {
        'fullname': fields.Str(required=True, validate=[
            validate_required, 
            validate.Regexp(r'^([a-zA-Z\s]{2,255})$', error='Full name must have 2 characters maximum. Only letters and spaces are allowed')
        ]),
        'gender': fields.Str(validate=[
            validate.OneOf(choices=['m', 'f'], error="Invalid choice")
        ], missing=None),
        'dob': fields.Date(missing=None),
        'address': fields.Str(required=True, validate=[validate_required]),
        'location_id': fields.UUID(missing=None),
        'timezone': fields.Str(validate=[
            validate.OneOf(choices=['Asia/Jakarta', 'Asia/Makassar', 'Asia/Jayapura'], error="Invalid choice")
        ], missing='Asia/Jakarta'), 
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @current_user()
    @permission_required('user', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload):

        user_id = request.headers.get('X-Consumer-Custom-Id')
        fullname = bleach.clean(payload['fullname'], strip=True)
        role = payload['role']
        gender = payload['gender']
        dob = payload['dob']
        address = bleach.clean(payload['address'], strip=True) 
        location = payload['location']
        timezone = payload['timezone']

        user = UserModel.get_by(user_id)

        if not user:
            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['User not found or has been deleted']
                    }
                }
            }), 404)

        user.profile.fullname = fullname
        user.profile.gender = gender
        user.profile.dob = dob
        user.profile.address = address
        user.profile.location_id = location_id
        user.profile.timezone = timezone

        try:

            db.session.commit()

            schema = UserSchema(only=args['only'], exclude=args['exclude'])
            result = schema.dump(user).data

            return make_response(jsonify({
                'success': True,
                'message': '',
                'data': {
                    'user': result
                }
            }), 200)

        except Exception as e:
            db.session.rollback()

            return make_response(jsonify({
                'success': False,
                'code': 400,
                'messages': {
                    'user': ['Something went wrong. Please try again later.']
                }
            }), 400)

api.add_resource(UserProfile, '/users/profile')
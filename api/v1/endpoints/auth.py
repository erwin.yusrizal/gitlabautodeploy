# -*- coding: utf-8 -*-
import arrow
import bleach
import time
import re
import uuid
import jwt

import requests

from ua_parser import user_agent_parser as ua_parser

from datetime import (
    datetime,
    timedelta
)
from sqlalchemy.sql.expression import func, and_, or_
from flask import (
    g,
    current_app,
    jsonify,
    request,
    make_response
)
from flask_restful import Resource
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    use_kwargs,
    parser
)
from api import cache, db
from api.v1 import api
from api.v1.models.users import(
    PermissionModel,
    RoleModel,
    UserModel,
    ProfileModel,
    BankAccountModel
)
from api.v1.schemas.users import(
    PermissionSchema,
    RoleSchema,
    UserSchema,
    ProfileSchema,
    BankAccountSchema
)
from api.utils import (
    normalize_phone_number,
    denormalize_phone_number,
    parse_verification_token
)
from api.utils.pagination import (
    Pagination
)

from api.tasks import (
    glad_user_send_email_queue, 
    glad_user_send_sms_queue
)

class Register(Resource):

    '''
    @method POST
    @endpoint /v1/auth/register
    @return 
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields')  

    def validate_phone(value):
        phone_match = re.search(r'^\(?(?:\+628|628|08)(?:\d{2,3})?\)?\d{2,4}?\d{2}?\d{2}$', value)

        if phone_match is None:
            raise ValidationError('Invalid phone number') 

    post_args = {
        'fullname': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z\s]{2,255})$', error='Full name must have 2 characters maximum. Only letters and spaces are allowed')
        ]),
        'phone': fields.Str(required=True, validate=[validate_required, validate_phone]),
        'email': fields.Email(required=True, validate=[validate_required]),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Register '''

        fullname = bleach.clean(payload['fullname']) 
        phone = normalize_phone_number(payload['phone'])
        email = bleach.clean(payload['email'].lower(), strip=True)

        user = UserModel.query.filter(or_(UserModel.phone==phone, UserModel.email==email)).first()                  

        if user:
            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 409,
                    'messages': {
                        'identity': ['Another user with this phone number or email address already exist']
                    }
                }
            }), 409)

        user_id = uuid.uuid4()

        u = UserModel()
        u.id = user_id
        u.email = email
        u.phone = phone
        u.status = 'unverified'
        u.role_id = 'd7d56ea1-3cc1-42cc-87f5-b162c46f7225'

        db.session.add(u)

        profile_id = uuid.uuid4()

        profile = ProfileModel()
        profile.id = profile_id
        profile.fullname = fullname
        profile.user_id = user_id

        db.session.add(profile)

        # create kong consumer account
        consumer_resp = requests.post(current_app.config.get('KONG_API_URL') + '/consumers', data={
            "username": email,
            "custom_id": str(user_id)
        })

        if not consumer_resp.ok:

            db.session.rollback()

            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'register': ['Something went wrong. Please try again later']
                    }
                }
            }), 422)

        consumer_json = consumer_resp.json()

        # assign jwt to the new user
        consumer_jwt_resp = requests.post(current_app.config.get('KONG_API_URL') + '/consumers/'+consumer_json['id'] + '/jwt')

        if not consumer_jwt_resp.ok:
            
            db.session.rollback()

            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'jwt': ['Something went wrong. Please try again later']
                    }
                }
            }), 422)

        consumer_jwt_json = consumer_jwt_resp.json()

        u.public_key = consumer_jwt_json['key']
        u.private_key = consumer_jwt_json['secret']

        db.session.commit()

        #add acl group permissions
        permissions = u.user_role.permissions
        for module in permissions:
            for (key, value) in permissions[module].items():
                if value > 0:
                    consumer_group_resp = requests.post(current_app.config.get('KONG_API_URL') + '/consumers/'+consumer_json['id'] + '/acls', data={
                        'group': module+':'+key
                    })

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        user = schema.dump(u).data

        # send a welcome email
        glad_user_send_email_queue.apply_async(kwargs={
            'to': u.email,
            'subject': 'Welcome to Glad.id',
            'template': 'email/welcome',
            'variables': {
                'fullname': u.profile.fullname
            }
        }, countdown=30, queue="GLADUSER")

        
        return make_response(jsonify({
            'success': True,
            'message': 'Congratulation! Your account has been registered',
            'data': {}                
        }), 201)


api.add_resource(Register, '/auth/register')


class Login(Resource):

    '''
    @method POST
    @endpoint /v1/auth/login
    @return verification code
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields')

    post_args = {
        'identity': fields.Str(required=True, validate=[validate_required]),
        'ui': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['dashboard', 'mobile'], error='Invalid choice.')
        ]),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Login '''

        identity = payload['identity']
        ui = payload['ui']

        phone_match = re.search(r'^\(?(?:\+628|628|08)(?:\d{2,3})?\)?\d{2,4}?\d{2}?\d{2}$', identity)
        email_match = re.match(r'^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', identity)

        if phone_match:
            identity = normalize_phone_number(identity)
            user = UserModel.query.filter(UserModel.phone == identity).first() 
        elif email_match:
            user = UserModel.query.filter(UserModel.email==identity).first()

        if not user:
            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'login': ['Invalid phone number or email address']
                    }
                }
            }), 422)

        if ui == 'dashboard' and user.user_role.slug not in ['root']:

            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 403,
                    'messages': {
                        'permission': ['You do not have permission to access this resource']
                    }
                }
            }), 403)

        elif ui == 'mobile' and user.role.slug not in ['user', 'driver']:

            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 403,
                    'messages': {
                        'permission': ['You do not have permission to access this resource']
                    }
                }
            }), 403)

        verification_code = user.verify()
        return make_response(jsonify({
            'success': True,
            'message': '',
            'data': {
                'verification': verification_code
            }                
        }), 200)


api.add_resource(Login, '/auth/login')


class Verify(Resource):

    '''
    @method POST
    @endpoint /v1/auth/verify
    @return user, token
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError('Please fill all required fields')   

    post_args = {
        'code': fields.Str(required=True, validate=[validate_required]),
        'passkey': fields.Str(required=True, validate=[validate_required]),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Verification '''

        code = payload['code']
        passkey = payload['passkey']

        data = parse_verification_token(code)

        list_keys = ['module', 'signature', 'expire']

        if not any(p in list_keys for p in data) or (data['expire'] <= int(time.time())):
            return make_reponse(jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'verification': ['Invalid verification code or has expired']
                    }
                }
            }), 422)

        else:

            u = UserModel.query.filter(
                UserModel.passkey==str(passkey), 
                UserModel.verification_code==str(data['signature']), 
                UserModel.passkey_expire >= int(time.time())).first()  

            if u is None:
                return make_response(jsonify({
                    'success': False,
                    'errors': {
                        'code': 422,
                        'messages': {
                            'verification': ['Invalid OTP or OTP has expired']
                        }
                    }
                }), 422)

            else:  

                u.passkey = None
                u.passkey_expire = None
                u.verification_code = None

                if u.status == 'unverified':
                    u.status = 'verified'

                db.session.commit()              

                schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
                user = schema.dump(u).data

                # craft JWT

                tomorrow = datetime.now() + timedelta(days=1)
                tomorrow_expire = time.mktime(tomorrow.timetuple())

                payload = {
                    "exp": tomorrow_expire,
                    "sub": str(u.id),                    
                    "iat": arrow.get().timestamp,
                    "iss": u.public_key,
                    "name": u.profile.fullname,
                    "role": str(u.role_id)
                }

                token = jwt.encode(payload, u.private_key, algorithm='HS256')

                ip_info = requests.get('http://ipinfo.io')

                if ip_info.ok:
                    user_agent = ua_parser.Parse(request.headers.get("User-Agent"))
                    ip_info_json = ip_info.json()

                    device =  user_agent['user_agent']['family']
                    os = user_agent['os']['family']

                    if  user_agent['user_agent']['major'] is not None:
                        device += ' ' + user_agent['user_agent']['major']

                    if user_agent['os']['major'] is not None:
                        os += ' ' + user_agent['os']['major']

                    glad_user_send_email_queue.apply_async(kwargs={
                        'subject': 'Security Alert',
                        'to': u.email,
                        'template': 'email/login',
                        'variables': {
                            'fullname': u.profile.fullname,
                            'location': ip_info_json['city']+', '+ip_info_json['region'],
                            'device': device + ', ' + os
                        }
                    }, countdown=30, queue='GLADUSER')

                return make_response(jsonify({
                    'success': True,
                    'message': '',
                    'data': {
                        'user': user,
                        'token': token
                    }  
                }), 200)

    '''
    @method PUT
    @endpoint /v1/auth/verify
    @return verification code
    '''

    put_args = {
        'code': fields.Str(required=True, validate=[validate_required])
    }

    @use_args(put_args, locations=['json'])
    def put(self, payload):
        
        ''' Reverify '''        
        
        code = payload['code']

        data = parse_verification_token(code)
        list_keys = ['signature', 'expire']

        if not any(p in list_keys for p in data):
            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'verification': ['Invalid verification code or has expired']
                    }
                }
            }), 422)

        else:

            user = UserModel.query.filter_by(verification_code=data['signature']).first()
            if user is None:
                return make_response(jsonify({
                    'success': False,
                    'errors': {
                        'code': 422,
                        'messages': {
                            'verification': ['Invalid OTP code or has expired.']
                        }
                    }
                }), 422)
            else:
                
                verification_code = user.verify()
                return make_response(jsonify({
                    'success': True,
                    'message': '',
                    'data': {
                        'verification': verification_code
                    }
                }), 200)


api.add_resource(Verify, '/auth/verify')
import base64
import binascii
import datetime
import hashlib
import hmac
import json
import os
import re
import time
import uuid
from collections import OrderedDict

from flask import current_app
from sqlalchemy import asc, desc, exc, text
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.sql.expression import func, and_, or_
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy_utils import JSONType
from flask_sqlalchemy_caching import FromCache, RelationshipCache

from api import cache, db
from api.tasks import glad_user_send_email_queue, glad_user_send_sms_queue
from api.utils import get_clients_ip, generate_sms_token, normalize_phone_number


def generate_uuid():
    return str(uuid.uuid4())


class PermissionModel(db.Model):
    
    __tablename__ = 'glad_permissions'

    id = db.Column(UUID(as_uuid=True), primary_key=True, index=True, unique=True, nullable=False, default=generate_uuid)
    name = db.Column(db.String(50), unique=True,nullable=False)
    slug = db.Column(db.String(50), unique=True,nullable=False)
    description = db.Column(db.String(255), nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

    def __init__(self, **kwargs):
        super(PermissionModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Permission %r>' % self.name

    @classmethod
    def count_all(cls, q=None):
        
        query = cls.query

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, order=None, order_by=None, page=1, perpage=25):

        offset = (page - 1) * perpage
        direction = desc if order == 'desc' else asc
        
        query = cls.query

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        query = query.order_by(direction(getattr(cls, order_by))).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id):
        
        query = cls.query
        cached_query = query.options(FromCache(cache))
        return cached_query.get(id)



class RoleModel(db.Model):
    
    __tablename__ = "glad_roles"

    id = db.Column(UUID(as_uuid=True), primary_key=True, index=True, unique=True, nullable=False, default=generate_uuid)
    name = db.Column(db.String(100), unique=True, nullable=False)
    slug = db.Column(db.String(100), unique=True, nullable=False)
    description = db.Column(db.String(255), nullable=True)
    permissions = db.Column(MutableDict.as_mutable(JSONType), nullable=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    users = db.relationship('UserModel', backref='user_role', lazy='dynamic', cascade="all, delete-orphan")

    def __init__(self, **kwargs):
        super(RoleModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Role %r>' % self.name


    @classmethod
    def count_all(cls, q=None):
        
        query = cls.query

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, order=None, order_by=None, page=1, perpage=25):

        offset = (page - 1) * perpage
        direction = desc if order == 'desc' else asc
        
        query = cls.query

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        query = query.order_by(direction(getattr(cls, order_by))).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id=None, slug=None):
        
        query = cls.query

        if id:
            query = query.filter_by(id=id)

        if slug:
            query = query.filter_by(slug=slug)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()



class UserModel(db.Model):
    
    STATUS = OrderedDict([('unverified', 'Unverified'), ('verified', 'Verified'), ('banned', 'Banned')])

    __tablename__ = "glad_users"

    id = db.Column(UUID(as_uuid=True), primary_key=True, index=True, unique=True, nullable=False, default=generate_uuid)
    nid = db.Column(db.String(100), nullable=True)
    email = db.Column(db.String(45), nullable=False)
    phone = db.Column(db.String(25), nullable=False)
    passkey = db.Column(db.String(6), nullable=True)
    passkey_expire = db.Column(db.Integer, nullable=True)
    public_key = db.Column(db.String(100), nullable=False)
    private_key = db.Column(db.String(100), nullable=False)
    verification_code = db.Column(db.String(128), nullable=True)
    status = db.Column(db.Enum(*STATUS, name="user_status"), index=True, default='unverified')
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    role_id = db.Column(UUID(as_uuid=True), db.ForeignKey('glad_roles.id'), index=True)
    profile = db.relationship('ProfileModel', uselist=False, backref='profile_user', cascade='all, delete-orphan')


    def __init__(self, **kwargs):
        super(UserModel, self).__init__(**kwargs)


    def __repr__(self):
        return '<User %r, %r, %r>' % (self.id, self.email, self.phone)

    
    def check_permission(self, key, value):
        permissions = self.user_role.permissions
        if key in permissions and permissions[key][value]:
            return True
        return False


    def verify(self, params={}):

        nextminutes = datetime.datetime.now() + datetime.timedelta(minutes=5)
        passkey_expire = time.mktime(nextminutes.timetuple())
        passkey = generate_sms_token(self.phone)

        self.passkey = passkey
        self.passkey_expire = passkey_expire

        secret = str(time.time()*1000) + self.phone + get_clients_ip() + binascii.b2a_hex(os.urandom(100))
        signature = hmac.new(current_app.config.get('SECRET_KEY'), secret, hashlib.sha256).hexdigest()

        self.verification_code = signature
        db.session.commit()

        payload = {
            'module': 'user',
            'signature': signature,
            'expire': passkey_expire,
            'params': params
        }

        verification_code = base64.urlsafe_b64encode(json.dumps(payload))

        # if self.phone:
        #     glad_send_sms_queue.apply_async(kwargs={
        #     'phonenumber': self.phone,
        #     'content': passkey + ' is your Glad verification code.'
        #     }, countdown=60, queue="GLADUSER")

        # elif self.email:
        #     glad_send_email_queue(kwargs={
        #         'subject': 'Glad Verification Code',
        #         'to': self.email,
        #         'template': 'email/verification',
        #         'variables': {
        #             'fullname': self.profile.fullname,
        #             'smstoken': passkey,
        #             'expire': '5 minutes'
        #         }
        #     }, countdown=60, queue='GLADUSER') 


        return {
            'code': verification_code,
            'expired': passkey_expire
        }



    @classmethod
    def count_all(cls, q=None, status=None, role_id=None):
        
        query = cls.query

        if q:
            query = query.filter(or_(cls.fullname.like('%'+q+'%'), cls.phone.like('%'+q+'%'), cls.badge.like('%'+q+'%')))

        if status:
            query = query.filter(cls.status == status)

        if role_id:
            query = query.filter(cls.role_id == role_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, status=None, role_id=None, order=None, order_by=None, page=1, perpage=10):

        offset = (page - 1) * perpage
        direction = desc if order == 'desc' else asc
        
        query = cls.query        

        if q:
            query = query.filter(or_(cls.fullname.like('%'+q+'%'), cls.phone.like('%'+q+'%'), cls.badge.like('%'+q+'%')))
        
        if status:
            query = query.filter(cls.status == status)

        if role_id:
            query = query.filter(cls.role_id == role_id)

        query = query.order_by(direction(getattr(cls, order_by))).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id=None, phone=None, email=None):
        
        query = cls.query

        if id:
            query = query.filter(cls.id == id)

        if phone:
            query = query.filter(cls.phone == phone)

        if email:
            query = query.filter(cls.email == email)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()



class ProfileModel(db.Model):

    GENDER = OrderedDict([('m', 'Male'), ('f', 'Female')])

    TIMEZONE = OrderedDict([("Asia/Jakarta", "Asia/Jakarta"), ("Asia/Makassar", "Asia Makassar"), ("Asia/Jayapura", "Asia/Jayapura")])
      
    __tablename__ = 'glad_user_profiles'

    id = db.Column(UUID(as_uuid=True), primary_key=True, index=True, unique=True, nullable=False, default=generate_uuid)
    fullname = db.Column(db.String(45), nullable=False)
    gender = db.Column(db.Enum(*GENDER, name='user_gender'), nullable=True)
    dob = db.Column(db.Date, nullable=True)
    address = db.Column(db.String(255), nullable=True)
    balance = db.Column(db.Integer, default=0)
    point = db.Column(db.Integer, default=0)
    timezone = db.Column(db.Enum(*TIMEZONE, name='user_timezone'), default='Asia/Jakarta')
    user_id = db.Column(UUID(as_uuid=True), db.ForeignKey('glad_users.id', ondelete='CASCADE'), index=True)
    location_id = db.Column(UUID(as_uuid=True), nullable=True)
    photo_id = db.Column(UUID(as_uuid=True), nullable=True)

    def __init__(self, **kwargs):
        super(ProfileModel, self).__init__(**kwargs)


    def __repr__(self):
        return '<Profile %r, %r>' % (self.id, self.fullname)


class BankAccountModel(db.Model):

    __tablename__ = 'glad_user_bank_accounts'

    id = db.Column(UUID(as_uuid=True), primary_key=True, index=True, unique=True, nullable=False, default=generate_uuid)
    account_name = db.Column(db.String(45), nullable=False)
    account_number = db.Column(db.String(45), nullable=False)
    branch = db.Column(db.String(45), nullable=True)    
    user_id = db.Column(UUID(as_uuid=True), db.ForeignKey('glad_users.id', ondelete='CASCADE'), index=True)
    bank_id = db.Column(UUID(as_uuid=True))

    def __init__(self, **kwargs):
        super(BankAccountModel, self).__init__(**kwargs)


    def __repr__(self):
        return '<BankAccount %r, %r, %r, %r>' % (self.id, self.account_name, self.account_number, self.bank_id)


    
    @classmethod
    def count_all(cls, q=None, bank_id=None):
        
        query = cls.query

        if q:
            query = query.filter(or_(cls.account_name.like('%'+q+'%'), cls.account_number.like('%'+q+'%')))

        if bank_id:
            query = query.filter(cls.bank_id == bank_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, bank_id=None, order=None, order_by=None, page=1, perpage=25):

        offset = (page - 1) * perpage
        direction = desc if order == 'desc' else asc
        
        query = cls.query

        if q:
            query = query.filter(or_(cls.account_name.like('%'+q+'%'), cls.account_number.like('%'+q+'%')))

        if bank_id:
            query = query.filter(cls.bank_id == bank_id)

        query = query.order_by(direction(getattr(cls, order_by))).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id):
        
        query = cls.query
        cached_query = query.options(FromCache(cache))
        return cached_query.get(id)
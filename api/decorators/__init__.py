import datetime
import time

from functools import update_wrapper, wraps
from flask import request, jsonify, g, make_response

from flask_restful import abort

from api.v1.models.users import UserModel


def anonymous_required():
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if 'Authorization' in request.headers:
                return make_response(jsonify({
                    'success': False,
                    'errors': {
                        'code': 403,
                        'messages': {
                            'forbidden': ['You do not have permission to access this resource']
                        }
                    }
                }), 403)
            return f(*args, **kwargs)
        return decorated_function
    return decorator


def permission_required(permission, action):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            user_id = request.headers.get('X-Consumer-Custom-Id')

            if user_id is None:
                return make_response(jsonify({
                    'errors': {
                        'code': 403,
                        'messages': {
                            'forbidden': ['You do not have permission to access this resource']
                        }
                    }
                }), 403)

            user = UserModel.query.get(user_id)
            if user and not user.check_permission(permission, action):
                return make_response(jsonify({
                    'errors': {
                        'code': 403,
                        'messages': {
                            'forbidden': ['You do not have permission to access this resource']
                        }
                    }
                }), 403)
            return f(*args, **kwargs)
        return decorated_function
    return decorator


def current_user():
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            user_id = request.headers.get('X-Consumer-Custom-Id')
            if user_id is None:
                return make_response(jsonify({
                    'success': False,
                    'errors': {
                        'code': 401,
                        'messages': {
                            'authorization': [ 'You do not have permission to access this resource using credentials that you supplied']
                        }
                    }
                }), 401)
                
            user = UserModel.query.get(user_id)
            if user is None:
                return make_response(jsonify({
                    'success': False,
                    'errors': {
                        'code': 401,
                        'messages': {
                            'authorization': [ 'You do not have permission to access this resource using credentials that you supplied']
                        }
                    }
                }), 401)
            g.current_user = user
            return f(* args, **kwargs)
        return decorated_function
    return decorator
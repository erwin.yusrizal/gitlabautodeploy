import random

from flask import current_app, render_template
from flask_mail import Message

from api import mail, celery

from api.utils import (
    normalize_phone_number
)

def exponential_backoff(task_self):
    minutes = task_self.default_retry_delay / 60
    rand = random.uniform(minutes, minutes * 1.3)
    return int(rand ** task_self.request.retries) * 60


'''
Send Celery Email Task Handler
'''

@celery.task(name='glad.user.api.tasks.send_email_queue', bind=True, max_retries=5, acks_late=True, default_retry_delay=10)
def glad_user_send_email_queue(self, *args, **kwargs):

    message = args if args else kwargs

    if type(message) == tuple:
        message = message[0]

    email = Message(message["subject"], sender=("Help Center", "helpcenter@setali.id"), recipients=[message["to"]])
    # email.body = render_template(message["template"] + '.txt', message=message["variables"])
    email.html = render_template(message["template"] + '.html', message=message["variables"])

    if 'attachment' in message['variables']:
        if message['variables']['attachment']:
            email.attach(message['variables']['attachment'], 'application/octect-stream', open(message['variables']['attachment'], 'rb').read())

    try:
        mail.send(email)
    except Exception as e:
        self.retry(exc=e, countdown=exponential_backoff(self))



'''
Send SMS Task Handler
'''

@celery.task(name='glad.user.api.tasks.send_sms_queue', bind=True, max_retries=5, acks_late=True, default_retry_delay=10)
def glad_user_send_sms_queue(self, *args, **kwargs):

    message = args if args else kwargs

    if type(message) == tuple:
        message = message[0]
    
    
    phone = normalize_phone_number(message["phonenumber"]) if message["phonenumber"] else None

    if phone:
        try:
            resp = client.send_message({
                'from': 'glad.id',
                'to': phone.encode("UTF-8"),
                'text': message["content"].encode("UTF-8"),
                'ttl': 120000
            })

            resp = resp['messages'][0]
            if resp['status'] == '0':
                return dict(success=True, message=dict(message_id=resp['message-id'], balance=resp["remaining-balance"]))
            else:
                return dict(success=False, message=resp["error-text"])
        except Exception as e:
            self.retry(exc=e, countdown=exponential_backoff(self))

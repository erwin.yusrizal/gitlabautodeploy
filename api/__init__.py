from flask import Flask
from flask_caching import Cache
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_mail import Mail
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy, Model
from flask_sqlalchemy_caching import CachingQuery

from celery import Celery
from config import config, Config

Model.query_class = CachingQuery

cache = Cache()
mail = Mail()
db = SQLAlchemy(session_options={'query_cls': CachingQuery})
limiter = Limiter(key_func=get_remote_address)
marshmallow = Marshmallow()

celery = Celery(__name__, broker=Config.CELERY_BROKER_URL, include=['api.tasks'])

def create_app(cfg):

    app = Flask(__name__)
    app.config.from_object(config[cfg])
    config[cfg].init_app(app)

    cache.init_app(app)
    db.init_app(app)
    mail.init_app(app)
    marshmallow.init_app(app)
    limiter.init_app(app)
    celery.conf.update(config)

    from api.v1 import blueprint as api_module

    app.register_blueprint(api_module)

    return app



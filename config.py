import os
import redis
from datetime import timedelta

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):

    SERVER_NAME = 'octo-user.setali.id'
    
    #Flask
    DEBUG = False
    TESTING = False
    SQLALCHEMY_ECHO = False
    FLASK_COVERAGE = True
    PROPAGATE_EXCEPTIONS = True

    ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'xls', 'xlsx'])
    MAX_CONTENT_LENGTH = 20 * 1024 * 1024

    ERROR_404_HELP = False

    # BLEACH
    BLEACH_ALLOWED_TAGS = set(['a', 'b', 'em', 'i', 'li', 'ol', 'strong', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'p'])
    BLEACH_ALLOWED_ATTRIBUTES = ['href', 'title', 'style', 'src']
    BLEACH_ALLOWED_STYLES = ['font-family', 'font-weight']
    BLEACH_STRIP_TAGS = True
    
    SECRET_KEY = '\t\xec\x90;\xd2/\xbd\xe7\xa7\x16v\x10h\xae\x99\x16\x1a\xb9\xae\xc3A,x\x9f?\xed#\xc7]Td\x8c}<\x9dx\xb3|\x01\xfc@\xf0\\rs\xcb\x1b\x1d\xca(\x1ct]\xf4O\xa91n\x17}\x92\xb0uJ.\xf7k\xa5\xa6\xa2N\xf7\xe3\xf1\xa96q\x96\xefL\xe6\xa7\x9a\x1e\xe6\xdc\xb6\xd8\xd1\rG\x19\xe7\xde\xaftCt\xf5\x19'

    #CACHE
    CACHE_TYPE = 'redis'
    CACHE_DEFAULT_TIMEOUT = 60
    CACHE_REDIS_URL = 'redis://:root@0.0.0.0:6379/1'
    CACHE_KEY_PREFIX = 'glad_cache_'

    # RATE LIMIT
    RATELIMIT_HEADERS_ENABLED = True
    RATELIMIT_STORAGE_URL = 'redis://:root@0.0.0.0:6379/1'
    RATELIMIT_KEY_PREFIX = 'glad_ratelimit_'

    # CELERY
    CELERY_BROKER_URL = 'redis://:root@0.0.0.0:6379/1'
    CELERY_RESULT_BACKEND = 'redis://:root@0.0.0.0:6379/1'
    CELERY_ACCEPT_CONTENT = ['json', 'pickle']

    # MAIL
    ADMIN_EMAIL = 'mail@example.com'
    MAIL_SERVER = 'smtp.example.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = 'mail@example.com'
    MAIL_PASSWORD = 'secret'
    MAIL_DEFAULT_SENDER = 'mail@example.com'
    MAIL_ASCII_ATTACHMENTS = True
    MAIL_DEBUG = False

    CORS_ENABLED = True
    CORS_ALLOW_HEADERS = ['Origin', 'Accept', 'Content-Type', 'Authorization', 'Content-Length', 'X-Requested-With']
    CORS_METHODS = ['GET', 'POST', 'DELETE', 'PUT', 'OPTIONS']
    CORS_SUPPORTS_CREDENTIALS = True

    # ONE SIGNAL
    ONESIGNAL_APPID = 'da733a06-23ca-4554-aaac-a032e9eb35ca'
    ONESIGNAL_APIKEY = 'ZWJjMDkwMzAtODYzMC00NmM3LTg0Y2UtNzU1YTlkMzM4MTk2'
    ONESIGNAL_APIURL = 'https://onesignal.com/api/v1/notifications'
    ONESIGNAL_CHANNELID = '598b79c9-6748-4cf9-99c2-4e5fd94f26d9'
    
    # NEXMO
    NEXMO_API_KEY = 'b824ced9'
    NEXMO_SECRET_KEY = '5e560391a01394e0'

    # KONG
    KONG_API_URL = 'https://api.setali.id'

    # SERVICES
    MEDIA_SERVICE_URL = 'https://0.0.0.0:3000/v1'

    @staticmethod
    def init_app(app):
        pass


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:@0.0.0.0:5432/glad'

class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:@0.0.0.0:5432/glad-dev'


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:@0.0.0.0:5432/glad-test'


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig,
    'default': DevelopmentConfig
}

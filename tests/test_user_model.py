import unittest
import uuid

from api import create_app, db
from api.v1.models.users import (
    PermissionModel,
    RoleModel,
    UserModel
)
from api.utils import generate_unique_id

class AuthTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()

        db.create_all()

        ''' create permissions '''
        permission = PermissionModel()
        permission.name = 'User'
        permission.slug = 'user'

        db.session.add(permission)
        db.session.commit()

        ''' create roles '''
        roles = [{
            'name': 'Orphan',
            'slug': 'orphan',
            'permissions': {
                'permission': {
                    'create': 0,
                    'viewall': 0,
                    'viewown': 0,
                    'editown': 0,
                    'deleteown': 0,
                    'viewother': 0,
                    'editother': 0,
                    'deleteother': 0
                },
                'role': {
                    'create': 0,
                    'viewall': 0,
                    'viewown': 0,
                    'editown': 0,
                    'deleteown': 0,
                    'viewother': 0,
                    'editother': 0,
                    'deleteother': 0
                },
                'user': {
                    'create': 0,
                    'viewall': 0,
                    'viewown': 0,
                    'editown': 0,
                    'deleteown': 0,
                    'viewother': 0,
                    'editother': 0,
                    'deleteother': 0
                }
            }
        }, {
            'name': 'Root',
            'slug': 'root',
            'permissions': {
                'permission': {
                    'create': 1,
                    'viewall': 1,
                    'viewown': 1,
                    'editown': 1,
                    'deleteown': 1,
                    'viewother': 1,
                    'editother': 1,
                    'deleteother': 1
                },
                'role': {
                    'create': 1,
                    'viewall': 1,
                    'viewown': 1,
                    'editown': 1,
                    'deleteown': 1,
                    'viewother': 1,
                    'editother': 1,
                    'deleteother': 1
                },
                'user': {
                    'create': 1,
                    'viewall': 1,
                    'viewown': 1,
                    'editown': 1,
                    'deleteown': 1,
                    'viewother': 1,
                    'editother': 1,
                    'deleteother': 1
                }
            }
        }]

        for r in roles:
            role = RoleModel()
            role.name = r['name']
            role.slug = r['slug']
            role.permissions = r['permissions']

            db.session.add(role)

        db.session.commit()


    def tearDown(self):

        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    
    def test_orphan_permission(self):

        role = RoleModel.get_by(slug='orphan')

        user = UserModel()
        user.email = 'erwin@gmail.com'
        user.phone = '628123456789'
        user.status = 'verified'
        user.role_id = role.id
        user.public_key = generate_unique_id(32)
        user.private_key = generate_unique_id(32)

        db.session.add(user)
        db.session.commit()

        self.assertFalse(user.check_permission('user', 'create'))
        self.assertFalse(user.check_permission('user', 'viewall'))
        self.assertFalse(user.check_permission('user', 'viewown'))
        self.assertFalse(user.check_permission('user', 'editown'))
        self.assertFalse(user.check_permission('user', 'deleteown'))
        self.assertFalse(user.check_permission('user', 'viewother'))
        self.assertFalse(user.check_permission('user', 'editother'))
        self.assertFalse(user.check_permission('user', 'deleteother'))


    def test_root_permission(self):

        role = RoleModel.get_by(slug='root')

        user = UserModel()
        user.email = 'erwin@gmail.com'
        user.phone = '628123456789'
        user.status = 'verified'
        user.role_id = role.id
        user.public_key = generate_unique_id(32)
        user.private_key = generate_unique_id(32)

        db.session.add(user)
        db.session.commit()

        self.assertTrue(user.check_permission('user', 'create'))
        self.assertTrue(user.check_permission('user', 'viewall'))
        self.assertTrue(user.check_permission('user', 'viewown'))
        self.assertTrue(user.check_permission('user', 'editown'))
        self.assertTrue(user.check_permission('user', 'deleteown'))
        self.assertTrue(user.check_permission('user', 'viewother'))
        self.assertTrue(user.check_permission('user', 'editother'))
        self.assertTrue(user.check_permission('user', 'deleteother'))
